/**
 * Created by kimberlykristen on 28/3/16 TESTtest
 */
var express = require("express");
var app = express();


app.get("/",
    function (req, res) {

        //http status
        res.status(202);
        //mime type
        res.type("text/plain");

        res.send("The current time is " + (new Date()));
    }
);



app.get("/hello",
    function (req, res) {

        res.status(202);
        res.type("text/html");
        res.send("<u>HELLO</u>");
    }
);





//3000 - port number
app.listen(3000, function() {
    console.info("Application started on port 3000");
}

);

